package com.revotech.DynamicForm;

import com.revotech.DynamicForm.model.Form;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.ListModelList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormManagerVM {
    private ListModelList<Form> formList;

    @Init
    public void init() {
        Map<String, Object> arg = (Map<String, Object>) Executions.getCurrent().getArg();
        formList = (ListModelList<Form>) arg.get("formList");
    }

    @Command
    public void deleteForm(@BindingParam("item") Form item) {
        formList.remove(item);
    }

    @Command
    public void editItem(@BindingParam("item") Form item) throws CloneNotSupportedException {
        Map<String, Object> arg = new HashMap<>();
        arg.put("list", formList.getInnerList());
        if (null != item) {
            arg.put("item", item.clone());
            arg.put("mode", "edit");
        } else {
            arg.put("item", new Form());
            arg.put("mode", "add");
        }
        Executions.createComponents("Pages/newItem.zul", null, arg);
    }

    @GlobalCommand
    @NotifyChange("formList")
    public void reload(@BindingParam("list") List<Form> list){
        formList.clear();
        formList.addAll(list);
        BindUtils.postGlobalCommand(null, null, "reloadModel", null);
    }


    public ListModelList<Form> getFormList() {
        return formList;
    }
}
