package com.revotech.DynamicForm;

import com.revotech.DynamicForm.model.Category;
import com.revotech.DynamicForm.model.Form;
import com.revotech.DynamicForm.service.CategoryService;
import com.revotech.DynamicForm.service.DataService;
import com.revotech.DynamicForm.service.FormService;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MyViewModel {

    private ListModelList<Form> formList = new ListModelList<>();
    private Map<String, Object> data;

    @Init
    public void init() {
        data = new DataService().getData();
        formList.addAll(new FormService().getListForm());
        fillModelToFormList(-1, -1);
    }

    //Đệ quy
    private void fillModelToFormList(Integer id, Integer catId) {
        List<Form> listChildren = formList.stream().filter(f -> id.equals(f.getParentId()) && "combobox".equals(f.getType())).collect(Collectors.toList());
        if(listChildren.isEmpty()){
            return;
        } else {
            listChildren.forEach(f -> {
                CategoryService categoryService = new CategoryService();
                f.setModel(categoryService.findByParentIdAndGroupCode(catId, f.getCategoryGroupCode()));
                data.put(f.getKey(), null);
                fillModelToFormList(f.getId(), null);
            });
        }

    }

    @Command
    @NotifyChange("formList")
    public void onSelectCategory(@BindingParam("id") Integer id, @BindingParam("item") Comboitem item) {
        Category categoryItem = item.getValue();
        formList.forEach(f -> {
            if (id.equals(f.getId()) && "combobox".equals(f.getType())) {
                f.setSelected(categoryItem);
                data.put(f.getKey(), categoryItem.getName());
            }
        });
        fillModelToFormList(id, categoryItem.getId());
    }

    @Command
    public void onChangeDate(@BindingParam("k") String key, @BindingParam("v") Date value) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        data.put(key, null == value ? null : dateFormat.format(value));
    }

    @Command
    public void print() {
        StringBuilder sb = new StringBuilder();
        Messagebox.show(data.toString());
    }

    @Command
    public void newItem() {
        Map<String, Object> arg = new HashMap<>();
        arg.put("formList", formList);
        Executions.createComponents("Pages/FormManager.zul", null, arg);
    }

    @GlobalCommand
    @NotifyChange("formList")
    public void reloadModel() {
        fillModelToFormList(-1, -1);
    }

    public List<Form> getFormList() {
        return formList;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
