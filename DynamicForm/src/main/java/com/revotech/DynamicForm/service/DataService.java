package com.revotech.DynamicForm.service;

import java.util.HashMap;
import java.util.Map;

public class DataService {
    private Map<String, Object> data = new HashMap<>();

    public DataService() {
        data.put("CMND", "021364821");
        data.put("birthDay", "20/10/2000");
        data.put("book", "Tranh");
    }

    public Map<String, Object> getData() {
        return data;
    }
}
