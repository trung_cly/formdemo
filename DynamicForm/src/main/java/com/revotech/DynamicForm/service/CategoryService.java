package com.revotech.DynamicForm.service;

import com.revotech.DynamicForm.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CategoryService {
    private List<Category> categoryList = new ArrayList<>();

    public CategoryService() {
        categoryList.add(new Category(1, "Hà Nội", -1, "CITY"));
        categoryList.add(new Category(2, "TP.Hồ Chí Minh", -1, "CITY"));
        categoryList.add(new Category(3, "Đà Nẵng", -1, "CITY"));
        categoryList.add(new Category(4, "Quận Ba Đình", 1, "CITY"));
        categoryList.add(new Category(5, "Quận Hoàn Kiếm", 1, "CITY"));
        categoryList.add(new Category(6, "Quận Đống Đa", 1, "CITY"));
        categoryList.add(new Category(7, "Quận 1", 2, "CITY"));
        categoryList.add(new Category(8, "Quận 2", 2, "CITY"));
        categoryList.add(new Category(9, "Quận 3", 2, "CITY"));
        categoryList.add(new Category(10, "Quận 4", 2, "CITY"));
        categoryList.add(new Category(11, "Quận Hải Châu", 3, "CITY"));
        categoryList.add(new Category(12, "Quận Cẩm Lệ", 3, "CITY"));
        categoryList.add(new Category(13, "Quận Thanh Khê", 3, "CITY"));
        categoryList.add(new Category(14, "Quận Liên Chiểu", 3, "CITY"));
        categoryList.add(new Category(15, "Cống Vị", 4, "CITY"));
        categoryList.add(new Category(16, "Điện Biên", 4, "CITY"));
        categoryList.add(new Category(17, "Đội Cấn", 4, "CITY"));
        categoryList.add(new Category(18, "Chương Dương", 5, "CITY"));
        categoryList.add(new Category(19, "Cửa Đông", 5, "CITY"));
        categoryList.add(new Category(20, "Cửa Nam", 5, "CITY"));
        categoryList.add(new Category(21, "Cát Linh", 6, "CITY"));
        categoryList.add(new Category(22, "Hàng Bột", 6, "CITY"));
        categoryList.add(new Category(23, "Khâm Thiên", 6, "CITY"));
        categoryList.add(new Category(24, "Bến Nghé", 7, "CITY"));
        categoryList.add(new Category(25, "Bến Thành", 7, "CITY"));
        categoryList.add(new Category(26, "An Khánh", 8, "CITY"));
        categoryList.add(new Category(27, "An Đông Lợi", 8, "CITY"));
        categoryList.add(new Category(28, "Phường 1", 9, "CITY"));
        categoryList.add(new Category(29, "Phường 2", 9, "CITY"));
        categoryList.add(new Category(30, "Phường 1", 10, "CITY"));
        categoryList.add(new Category(31, "Phường 2", 10, "CITY"));
        categoryList.add(new Category(32, "Bình Hiên", 11, "CITY"));
        categoryList.add(new Category(33, "Bình Thuận", 11, "CITY"));
        categoryList.add(new Category(34, "Hòa An", 12, "CITY"));
        categoryList.add(new Category(35, "Hòa Phát", 12, "CITY"));
        categoryList.add(new Category(36, "An Khê", 13, "CITY"));
        categoryList.add(new Category(37, "Chính Gián", 13, "CITY"));
        categoryList.add(new Category(38, "Hòa Hiệp Bắc", 14, "CITY"));
        categoryList.add(new Category(39, "Hòa Hiệp Nam", 14, "CITY"));
        categoryList.add(new Category(40, "Tranh", -1, "BOOK"));
        categoryList.add(new Category(41, "Ảnh", -1, "BOOK"));
        categoryList.add(new Category(42, "Sách", -1, "BOOK"));
        categoryList.add(new Category(43, "Truyện", -1, "BOOK"));
    }

    public List<Category> findByParentIdAndGroupCode(Integer parentId, String groupCode) {
        if (null == parentId) {
            return new ArrayList<>();
        }
        return categoryList.stream().filter(c -> parentId.equals(c.getParentId()) && groupCode.equals(c.getGroupCode())).collect(Collectors.toList());
    }
}
