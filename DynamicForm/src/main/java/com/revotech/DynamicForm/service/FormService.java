package com.revotech.DynamicForm.service;

import com.revotech.DynamicForm.model.Form;

import java.util.ArrayList;
import java.util.List;

public class FormService {
    private List<Form> listForm = new ArrayList<>();

    public FormService() {
        listForm.add(new Form(1, "textbox", "CMND", "CMND/Hộ chiếu", -1, "CMND/Hộ chiếu", null));
        listForm.add(new Form(2, "combobox", "city", "Tỉnh/Thành phố", -1, "Tỉnh/Thành phố", "CITY"));
        listForm.add(new Form(3, "datebox", "birthDay", "Ngày sinh", -1, "Ngày sinh", null));
        listForm.add(new Form(4, "combobox", "district", "Quận/Huyện", 2, "Quận/Huyện", "CITY"));
        listForm.add(new Form(5, "textbox", "test", "dad", -1, null, null));
    }

    public List<Form> getListForm() {
        return listForm;
    }
}
