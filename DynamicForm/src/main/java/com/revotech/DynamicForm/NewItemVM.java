package com.revotech.DynamicForm;

import com.revotech.DynamicForm.model.Form;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Window;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewItemVM {
    private ListModelList<Form> formList = new ListModelList<>();
    private Form item;
    private String mode;

    @Init
    public void init() {
        Map<String, Object> arg = (Map<String, Object>) Executions.getCurrent().getArg();
        formList.addAll((List<Form>) arg.get("list"));
        item = (Form) arg.get("item");
        mode = (String) arg.get("mode");
        selectItemInList();
    }

    @Command
    @NotifyChange("item")
    public void onSelectType(@BindingParam("item") String type) {
        item.setType(type);
    }

    @Command
    public void onSelectParent(@BindingParam("item") Comboitem comboitem) {
        Form selected = comboitem.getValue();
        if (null != selected) {
            item.setParentId(selected.getId());
        } else {
            item.setParentId(-1);
        }
    }

    @Command
    public void clearParent(@BindingParam("combobox") Combobox cbb) {
        item.setParentId(-1);
        cbb.setSelectedIndex(-1);
    }

    @Command
    public void onSelectGroupCode(@BindingParam("item")String groupCode){
        item.setCategoryGroupCode(groupCode);
    }

    @Command
    public void onSave(@BindingParam("win") Window win) {
        if (null == item.getParentId()) {
            item.setParentId(-1);
        }
        if ("add".equals(mode)) {
            Integer maxId = formList.stream().map(Form::getId).max(Integer::compareTo).orElse(0);
            item.setId(maxId + 1);
            formList.add(item);
        } else {
            for (int i = 0; i < formList.size(); i++) {
                if (formList.get(i).getId().equals(item.getId())) {
                    formList.set(i, item);
                }
            }
        }
        Map<String, Object> arg = new HashMap<>();
        arg.put("list", formList.getInnerList());
        BindUtils.postGlobalCommand(null, null, "reload", arg);
        win.onClose();
    }

    private void selectItemInList() {
        if (null != item.getParentId()) {
            Form selected = formList.stream().filter(f -> f.getId().equals(item.getParentId())).findFirst().orElse(null);
            formList.setSelection(Collections.singleton(selected));
        }
    }

    public ListModelList<Form> getFormList() {
        return formList;
    }

    public Form getItem() {
        return item;
    }

    public void setItem(Form item) {
        this.item = item;
    }
}
