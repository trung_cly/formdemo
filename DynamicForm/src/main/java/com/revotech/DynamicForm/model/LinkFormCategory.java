package com.revotech.DynamicForm.model;

public class LinkFormCategory {
    private Integer formId;
    private Integer categoryId;

    public LinkFormCategory(Integer formId) {
        this.formId = formId;
    }

    public LinkFormCategory(Integer formId, Integer categoryId) {
        this.formId = formId;
        this.categoryId = categoryId;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
