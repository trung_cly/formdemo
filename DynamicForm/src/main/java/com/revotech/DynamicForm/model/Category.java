package com.revotech.DynamicForm.model;

public class Category {
    private Integer id;
    private String name;
    private Integer parentId;
    private String groupCode;

    public Category() {
    }

    public Category(Integer id, String name, Integer parentId, String groupCode) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.groupCode = groupCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }
}
