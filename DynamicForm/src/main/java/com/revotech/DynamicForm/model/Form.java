package com.revotech.DynamicForm.model;

import java.util.List;

public class Form implements Cloneable{
    private Integer id;
    private String type;
    private String key;
    private String label;
    private Integer parentId;
    private String placeholder;
    private String categoryGroupCode;
    private List<Category> model;
    private Category selected;

    public Form(Integer id, String type, String key, String label, Integer parentId, String placeholder, String categoryGroupCode) {
        this.id = id;
        this.type = type;
        this.key = key;
        this.label = label;
        this.parentId = parentId;
        this.placeholder = placeholder;
        this.categoryGroupCode = categoryGroupCode;
    }

    public Form() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getCategoryGroupCode() {
        return categoryGroupCode;
    }

    public void setCategoryGroupCode(String categoryGroupCode) {
        this.categoryGroupCode = categoryGroupCode;
    }

    public List<Category> getModel() {
        return model;
    }

    public void setModel(List<Category> model) {
        this.model = model;
    }

    public Category getSelected() {
        return selected;
    }

    public void setSelected(Category selected) {
        this.selected = selected;
    }

    public String toString() {
        return "{ "
                + "id: " + this.id + ", "
                + "label: " + this.label + ", "
                + "key: " + this.key + ", "
                + "type: " + this.type + ", "
                + "parentId: " + this.parentId + ", "
                + "placeholder: " + this.placeholder
                + " }";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}